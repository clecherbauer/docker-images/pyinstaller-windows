#!/usr/bin/env bash
set -e
. /root/.bashrc


if [ $REQUIREMENTS_TXT == "" ]; then
   REQUIREMENTS_TXT="requirements.txt"
fi

if [ -f $REQUIREMENTS_TXT ]; then
    pip install -r $REQUIREMENTS_TXT
fi

shift;
exec pyinstaller "$@"
